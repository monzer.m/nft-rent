import express  from "express";
import controller from "../controllers/NFTListingController"

const router = express.Router();
router.post('/list-my-Nft-for-rent', controller.listMyNftForRent);
router.get('/get-listed-NFTs-for-rent', controller.getListedNFTsForRent);

export = router;
