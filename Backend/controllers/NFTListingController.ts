import {Request, Response, NextFunction} from 'express';
import logging from '../source/config/logging';
import IListNFTForRentRequest from '../models/IListNFTForRentRequest';

const NAMESPACE = 'NFT Listing Controller'
const listeNFts : IListNFTForRentRequest[] = [];

const listMyNftForRent = (req : Request,  res : Response, next : NextFunction) => {
    logging.info(NAMESPACE, `listMyNftForRent called`);

    const body = <IListNFTForRentRequest> req.body;
    listeNFts.push(body);
    return res.status(200).json({
        message : 'done'
    });
}


const getListedNFTsForRent = (req : Request,  res : Response, next : NextFunction) => {
    logging.info(NAMESPACE, `getListedNFTsForRent called`);
    return res.status(200).json(listeNFts);
}


export default{listMyNftForRent, getListedNFTsForRent};