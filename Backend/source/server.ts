import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import logging from './config/logging';
import config from './config/config';
import myRoutes from '../routes/NFTListingRoutes';

const NAMESPACE = 'Server';
const router = express();


/* Logging */
router.use ((req, res, next) => {
    logging.info(NAMESPACE, `METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`);

    res.on('finish', () => {
         /** Log the res */
         logging.info(NAMESPACE, `METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`);
    })

    next();
})

/** Parse the request */
router.use(bodyParser.urlencoded({extended : false}));
router.use(bodyParser.json());

/** Rules of API */
router.use((req, res, next) =>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if(req.method == 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST PUT');
        return res.status(200).json({});
    }
    next();
});


/** Routes */
router.use('/', myRoutes);

/** Error Handling */
router.use((req, res, next) => {
    const error = new Error('not found');
    return res.status(404).json({
        message : error.message
    })
}) 

/** create server */
const httpSerer = http.createServer(router);
httpSerer.listen(config.config.server.port, () => logging.info(NAMESPACE, `Server running ${config.config.server.hostname}:${config.config.server.port}`));

