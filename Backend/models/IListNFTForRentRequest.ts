interface IListNFTForRentRequest{
    nftAddress : string;
    nftId : Number;
    owner : string;
    dailyRentFee : Number;
     paymentToken : string; 
    expireTime : Number; 
}

export = IListNFTForRentRequest;