# Collateral-less NFT renting  system : 

## Introduction : 
This system was built to provide Collateral-less NFT renting  feature for NFT ecosystem. For many use cases users might need to benefit the utility from specific NFT collection for short period with out costing so much buying the NFT, so the best solution is to be able to rent NFT and benefit it’s  utilities. 

## How the system works ? 
![](./d1.PNG)
1-	First step the NFT owner has to make an on-chain tx approving the system to withdraw their NFTs.  
2-	Owner can list their NFTs to a centralized system, without paying any extra gas fees. While listing he has to sign his request with his private key in order to validate this request later on-chain and prevent un-authorized users from playing around.  
3-	NFT renters can browse the listed NFTs from off-chain app making HTTPs/Get calls.  
4-	Renter decides to rent one of the listings, and make a transaction to NFTRenter smart contract providing information about the listing plus rent duration.   
5-	System will withdraw the NFT from owner wallet to NFTRenter wallet as vault, and transfer the whole renting amount from renter wallet to owner wallet.   
6-	NFTRenter can approve to 3rd parties that the renter is now renting this NFT as long as the period is still valid.  
7-	Once renting period ends the owner has to make withdraw transaction to get his NFT back to his wallet.   
 
![](./d2.PNG)
![](./d3.PNG)
 
## What Technologies was used in the system:
1-	Node.js, Express.js and Typescript.js for Backend application.  
2-	Solidity, Hardhat, Ethers and Chai for on-chain app.  
3-	React.js, Ethers.js for front end.   

## Possible improvements : 
-	We can use meta-transactions to eliminate the first step and use less gas fees by the nft-owner. This transaction could be executed later with-by the renter tx or by bots.  
-	We could mint some ERC20 tokens to making rent-prove integration process easier.  


## POC  : 
Verified Smart Contract :   
https://polygonscan.com/address/0xf69187696192435AE68A625Ef1c7A330543677a4#code  

Successful transaction :   
https://polygonscan.com/tx/0xe8c564902712de6e369d80420249ba791e6d4e6634ff2b7b80bedf065d6f75ea  
