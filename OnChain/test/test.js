const {expect} = require('chai');
require("@nomiclabs/hardhat-web3");
//const signDaiPermit = require('eth-permit');
const eipUtils = require('./EIP712');

describe('TestCases', () => {
  let bUSD, myNFT, nftRenterContract, _nftOwner,_renter, chainId, _ownerPK,   _module, provider, nftOwnerSigner, renterSigner;


  beforeEach(async()=>{
    provider = waffle.provider; 
    chainId  = (await provider.getNetwork()).chainId;
    console.log(`chainId >> ${JSON.stringify(chainId)}`)

    let BUSDFactory = await ethers.getContractFactory('BUSD');
    let MyNFTFactory = await ethers.getContractFactory('MyNFT');
    let NFTRenter = await ethers.getContractFactory('NFTRenter');

    bUSD = await BUSDFactory.deploy();
    myNFT = await MyNFTFactory.deploy('nft_renting', 'MFI');
    nftRenterContract = await NFTRenter.deploy();
  
    [nftOwnerSigner, renterSigner, user3, user4, user5, _]  = await ethers.getSigners();
    _nftOwner = nftOwnerSigner.address;
    _renter = renterSigner.address;
    
    console.log(`_owner >> ${_nftOwner}`);
    _ownerPK = "ac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80";


  })

  it('Deposit Token', async () => {
    const nftId = 1;
    await bUSD.mint(_renter, web3.utils.toWei('1000'));
    await myNFT.mint(_nftOwner, nftId);
    
    console.log(`nftRenterContract > ${nftRenterContract.address}`);
    
    await myNFT.connect(nftOwnerSigner).approve(nftRenterContract.address, nftId);
    await bUSD.connect(renterSigner).approve(nftRenterContract.address, web3.utils.toWei('10000'));

    await assertBalance(bUSD, _renter,  web3.utils.toWei('1000'), 'busd-balanceOf-renter :'); 
    let expireTime = Math.floor(Date.now() / 1000) + ( 86400 * 10);
    await callRentNft(nftRenterContract, renterSigner, chainId, _ownerPK, myNFT.address, nftId, _nftOwner, bUSD.address, web3.utils.toWei('10'), expireTime);
    
    await assertBalance(bUSD, _renter,  web3.utils.toWei('980'), 'busd-balanceOf-renter :'); 
    await assertBalance(bUSD, _nftOwner,  web3.utils.toWei('20'), 'busd-balanceOf-nft-owner :'); 

    let newOwner = await myNFT.ownerOf(nftId);
    expect(newOwner).to.equal(nftRenterContract.address);
  })
  
   
})

async function assertBalance(tokenContract, address, val, printMsg){
  var balcnceWei = await tokenContract.balanceOf(address);
  var balcnceEth = `${ethers.utils.formatEther(`${balcnceWei}`)}`;
  console.log(`${printMsg} = ${balcnceEth}`);
  expect(balcnceWei).to.equal(val);
}


async function callRentNft(nftRenterContract, renterSigner, chainId, _ownerPK, _nftAddress, _nftId, _owner, _paymentToken, _dailyRentFee, _expireTime){
  let nftRentListing = {
     nftAddress :_nftAddress , 
     nftId : _nftId,
     owner : _owner, 
     dailyRentFee : _dailyRentFee,
     paymentToken: _paymentToken, 
     expireTime: _expireTime
  }

  let signature = eipUtils.signNFTRentListingEIPMsg(nftRentListing, chainId, nftRenterContract.address, _ownerPK);
  await nftRenterContract.connect(renterSigner).rentNft(nftRentListing, signature, 2);
}
