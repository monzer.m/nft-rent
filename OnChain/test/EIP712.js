const ethSigUtil = require('eth-sig-util');
const Wallet = require('ethereumjs-wallet').default;

  const version = '1';
  const name = 'NFTRenter';
  
  exports.signNFTRentListingEIPMsg =  function (nftRentListing, chainId, verifyingContract, privateKey) {
    const data = buildNFTRentListingEIPMsg(chainId, verifyingContract, nftRentListing);
    //console.log(`privateKey > ${privateKey} -- ${Uint8Array.from(privateKey)} -- ${Buffer.from(privateKey, 'hex')}`)
    const myWallet = Wallet.fromPrivateKey(Buffer.from(privateKey, 'hex'));
    return ethSigUtil.signTypedMessage(myWallet.getPrivateKey(), { data });
  }



  function buildNFTRentListingEIPMsg(chainId, verifyingContract, nftRentListing) {
    const EIP712Domain = [
      { name: 'name', type: 'string' },
      { name: 'version', type: 'string' },
      { name: 'chainId', type: 'uint256' },
      { name: 'verifyingContract', type: 'address' },
    ];

    return {
      primaryType: 'NFTRentListing',
      types: { EIP712Domain, NFTRentListing },
      domain: { name, version, chainId, verifyingContract },
      message: nftRentListing,
    };
  }


  const NFTRentListing = [
    { name: 'nftAddress', type: 'address' },
    { name: 'nftId', type: 'uint' },
    { name: 'owner', type: 'address' },
    { name: 'dailyRentFee', type: 'uint' },
    { name: 'paymentToken', type: 'address' },
    { name: 'expireTime', type: 'uint256' }
  ];
