pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";


contract BUSD is ERC20{
 constructor() ERC20('BUSD', 'BUSD') {

 }

 function mint(address to , uint tokenId) external{
    _mint(to, tokenId);
 }
 
}