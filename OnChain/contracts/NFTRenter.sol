pragma solidity ^0.8.0;
/*Developed By Monzer Masri */

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/draft-EIP712.sol";
import "@openzeppelin/contracts/utils/cryptography/SignatureChecker.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

import {StructsLib} from "./StructsLib.sol"; 

contract NFTRenter is EIP712('NFTRenter', '1'){

 mapping(bytes32 => bool) cancelledRentListings;
 mapping(address => mapping(uint => StructsLib.RentTerms)) nftRentTerms;

  constructor() public {
  }
 
  /**
    @function : Renter calls this function to rent an NFT. 
    @param rentListing : Rent listing info which was submitted by NFT owner to off-chain side. 
    @param signature : Signature provided by NFT-Owner once submiting listing-for-rent request.
    @param rentDays : Number of days renter wants to rent the NFT.
  */
  function rentNft(StructsLib.NFTRentListing memory rentListing, bytes calldata signature, uint rentDays) external {
    //require offer not expired and not cancelled. 
    uint rentDurationSec = (rentDays * 86400);
    require(block.timestamp + rentDurationSec < rentListing.expireTime, "This listing is expired!");

    bytes32 NftlistingHash = calcObjHash(rentListing);
    require(SignatureChecker.isValidSignatureNow(rentListing.owner, NftlistingHash, signature), "LOP: bad signature");
    require(!cancelledRentListings[NftlistingHash], "Rent-Listing was canceled.");
    
    ERC721(rentListing.nftAddress).transferFrom(rentListing.owner, address(this) , rentListing.nftId);

    uint totalPaymentAmount = rentDays * rentListing.dailyRentFee ;
    ERC20(rentListing.paymentToken).transferFrom(address(msg.sender),  rentListing.owner, totalPaymentAmount);
    
    uint rentEndTime = block.timestamp + rentDurationSec;
    nftRentTerms[rentListing.nftAddress][rentListing.nftId] = StructsLib.RentTerms(rentListing.nftAddress, rentListing.nftId,rentListing.owner , address(msg.sender), rentEndTime);

    emit StructsLib.NFTRented(NftlistingHash,rentListing.nftAddress, rentListing.nftId, msg.sender, rentListing.owner);
  }


  /**
    @function : NFT-owner calls this function to cancel previously submitted listing-for-rent request. 
    @param rentListing : Rent listing info which was submitted by NFT owner to off-chain side. 
    @param signature : Signature provided by NFT-Owner once submiting listing-for-rent request.
  */
  function cancelNFTRentListing(StructsLib.NFTRentListing memory rentListing, bytes calldata signature) external {
    //cancel 
    bytes32 NftlistingHash = calcObjHash(rentListing);
    require(SignatureChecker.isValidSignatureNow(rentListing.owner, NftlistingHash, signature), "LOP: bad signature");

    cancelledRentListings[NftlistingHash] = true;
  }
 
  function calcObjHash(StructsLib.NFTRentListing memory rentListing) public view returns(bytes32){
    return _hashTypedDataV4(keccak256(abi.encode(StructsLib.NFTRentListing_TYPEHASH, rentListing)));
  }

  /**
    @function : Checks if specific address is truely the renter of specific NFT. 
    @param renter : Address of account to check. 
    @param nftAddress : Address of NFT contract.
    @param nftId : Token ID of NFT asset.
  */
  function checkRenter(address renter, address nftAddress, uint nftId) public view returns(bool){
      if (block.timestamp >= nftRentTerms[nftAddress][nftId].endTime){
        return false;
      }

      return (nftRentTerms[nftAddress][nftId].renter == renter);
  } 

  function getRentTerms(address nftAddress, uint nftId) public view returns (StructsLib.RentTerms memory){
      return nftRentTerms[nftAddress][nftId];
  }

  /**
    @function : Withdraws an NFT from this contract returning it back to its owner after rent offer ends. 
    @param nftAddress : Address of NFT contract.
    @param nftId : Token ID of NFT asset.
  */
  function withdrawNFT(address nftAddress, uint nftId) public {
      require(block.timestamp < nftRentTerms[nftAddress][nftId].endTime, "Rent duration did'nt end yet!");
      ERC721(nftAddress).transferFrom(address(this), address(nftRentTerms[nftAddress][nftId].owner), nftId);     
  }
   
}