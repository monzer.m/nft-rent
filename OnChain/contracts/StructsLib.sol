pragma solidity ^0.8.0;

library StructsLib {
 
 
  bytes32 constant public NFTRentListing_TYPEHASH = keccak256(
        "NFTRentListing(address nftAddress,uint nftId,address owner,uint dailyRentFee,address paymentToken,uint256 expireTime)"
    );

  struct NFTRentListing{
    address nftAddress;
    uint nftId;
    address owner;
    uint dailyRentFee;
    address paymentToken; 
    uint256 expireTime; 
  }

  struct RentTerms {
    address nftAddress;
    uint nftId;
    address owner;
    address renter;
    uint256 endTime;
  }

  
  event NFTRented(
      bytes32 rentHash, 
      address nftAddress,
      uint nftId,
      address renter,
      address owner 
    );

}

