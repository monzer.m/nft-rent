/** @type import('hardhat/config').HardhatUserConfig */
require('@nomiclabs/hardhat-waffle');
require("@nomiclabs/hardhat-web3");
require("@nomiclabs/hardhat-etherscan"); 
let secrets = require("./secrets.json");

module.exports = {
  solidity: "0.8.9", 
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
     },
    rinkeby: {
      url: "https://eth-rinkeby.alchemyapi.io/v2/123abc123abc123abc123abc123abcde",
      accounts: ['f42cc4d59003a0de17a68d760ab0a3ddf4a64ba928a0dec0d389a1fa0ffa894b', 'd5cc27bfadb2131d73fc7524bb0d2c4bb2b0d939ad49cd6065426a72d1352f23']
    },
    matictest: {
      url: secrets.url,
      accounts: [secrets.private_key]
    } 
  },
  etherscan: {
    apiKey: 'WYR9UTC7PUX7GAAXVFB1DFH9GP8PFR94UT'
  },
}