async function main() {
  const nftId = 1;

  const [deployer] = await ethers.getSigners();
  console.log(`Deploying contracts with account : ${deployer.address}`);

  const balance = await deployer.getBalance();
  console.log(`Deployer Account Balance : ${balance.toString()}`);

  let provider = waffle.provider; 
  let network  = await provider.getNetwork()
  console.log(`chainId >> ${network.chainId}`);

  let BUSDFactory = await ethers.getContractFactory('BUSD');
  let MyNFTFactory = await ethers.getContractFactory('MyNFT');
  let NFTRenter = await ethers.getContractFactory('NFTRenter');


  let bUSD = await BUSDFactory.deploy();
  await bUSD.deployed();
  console.log(`Deployed BUSD >> ${bUSD.address}`);

  let myNFT = await MyNFTFactory.deploy('nft_renting', 'MFI_monzer');
  await myNFT.deployed();
  console.log(`Deployed myNFT >> ${myNFT.address}`);

  let nftRenterContract = await NFTRenter.deploy();
  await nftRenterContract.deployed();
  console.log(`Deployed nftRenterContract >> ${nftRenterContract.address}`);

  await bUSD.mint(deployer.address, web3.utils.toWei('1000000'));
  console.log(`bUSD.minted`);

  await myNFT.mint(deployer.address, nftId);
  console.log(`myNFT.minted`);
  await myNFT.approve(nftRenterContract.address, nftId);
  console.log(`myNFT.approve`);
  await bUSD.approve(nftRenterContract.address, web3.utils.toWei('10000000'));
  console.log(`bUSD.approve`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});